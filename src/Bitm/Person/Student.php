<?php
namespace App\Bitm\Person;

class Student{
    public $name="";
    public $id="";

    public  function __construct($data="Default")
    {
        $this->name=$data;

    }

    public function __destruct()
    {
        echo "<b>I am destructing<b>";
        echo "<hr>";
    }

    public function greetings(){
        echo "Hello from ".$this->name;
    }

}
